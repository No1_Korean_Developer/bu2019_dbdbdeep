<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="/views/header/header.jsp"%>
<title>페이지이름</title>

</head>
<body>

	<!-- Container ======================================================================================= -->
	<div class="cont_detail detail_product">
		<div class="inner_detail">


			<ul class="list_product">
				<c:forEach var="order" items="${orderList}">
					<li><a class="link_product"><span class="wrap_thumb">
								<img src="resources/img/${order.p_id}.jpg" class="thumb_g"
								alt="">
						</span><strong class="tit_product">${order.p_name}</strong> <em
							class="emph_price"> <span class="screen_out"></span> <span
								class="current_price">신청인 : ${order.u_name}, <span>신청
										: ${order.p_stock}개<br>
								</span>
							</span> 주소 : ${order.s_addr}<br> 신청 날짜 : ${order.o_date}
						</em></a></li>
				</c:forEach>
			</ul>
			<div>
				<input type="button" class="btn btn-default" value="뒤로가기"
					onclick="history.back(-1);">
			</div>
		</div>
	</div>

</body>

</html>