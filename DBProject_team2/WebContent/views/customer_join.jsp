<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<%@ include file="/views/header/header.jsp"%>
<title>고객 가입</title>
</head>
<body>

	<div align="center">
		<form action="${pageContext.request.contextPath}/regist.do"
			method="post">
			<table class="table">
				<tr>
					<th>휴대전화 번호</th>
					<td><input type="text" name="c_phone" maxlength=11
						class="form-control" type="text" value="" placeholder="휴대전화 "></td>
				</tr>
				<tr>
					<th>이름</th>
					<td><input type="text" name="c_name" maxlength=12
						class="form-control" type="text" value="" placeholder="이름"></td>
				</tr>
				<tr>
					<th>생년월일</th>
					<td><input type="text" name="c_birth" maxlength=8
						class="form-control" type="text" value="" placeholder="생년월일"></td>
				</tr>
				<tr>
					<th>패스워드</th>
					<td><input type="password" name="c_pass" maxlength=4
						class="form-control" type="text" value="" placeholder="패스워드 4자리"></td>
				</tr>
			</table>
			<br> <input class="btn btn-success" type="submit" value="회원등록">
			<input class="btn btn-warning" type="button" value="뒤로가기"
				onclick="history.back(-1);">
		</form>
	</div>


</body>
</html>