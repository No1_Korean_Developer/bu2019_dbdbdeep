<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<title>로그인</title>
</head>
<body>	
	<nav class="navbar navbar-default">
		<div class="container-fluid2">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapseds"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<div class="navbar-brand">
					<img src="resources/img/logo3.png" style="width: 200px; height:30px;">
				</div>

			</div>


			<ul class="nav navbar-nav navbar-right">
				<li>
					<div class="nav navbar-nav navbar-left">
						<c:choose>
							<c:when test="${loginedUser eq null }">
								<li><a
									href="${pageContext.request.contextPath}/login.do">Login</a>
								</li>
								<li><a
									href="${pageContext.request.contextPath}/join.do">Join</a>
								</li>
							</c:when>
							<c:otherwise>
								<li
									style="line-height: 21px; padding-top: 8px; margin-right: 12px">
									<b>${loginedUser.u_name}</b> 님 반갑습니다.
								</li>
								<li
									style="line-height: 21px; padding-top: 8px; margin-right: 12px">
									접속 매장번호 :<b>${loginedUser.u_store}</b>
								</li>
								<li><a
									href="${pageContext.request.contextPath}/logout.do">Logout</a>
								</li>
								<li><a
									href="${pageContext.request.contextPath}/DetailUser.do">마이페이지</a>
								</li>
							</c:otherwise>
						</c:choose>
					</div>
				</li>
			</ul>
		</div>
	</nav>

	<div>
	<br>
	<form action="${pageContext.request.contextPath}/login.do" method="post">
		<table class="table" align="center">
			<tr>
				<th>ID</th>
				<td><input id="u_id"  name="u_id" class="form-control" type="text" ></td>
			</tr>
			<tr>
				<th>Password</th>
				<td><input id="u_pass" name="u_pass" class="form-control" type="password" value=""></td>
			</tr>
		</table><br>
		<div align="center"><input class="btn btn-success" type="submit" value="로그인"></div>
	</form>
	<br>
	</div>
</body>
</html>
