<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="/views/header/header.jsp"%>
<title>카카오 프렌즈샵</title>

<style>

</style>

</head>
<body>
<div align="center">

<br>
	<table>
		<tr>
			<td width="100"><b>ID : </b></td>
			<td width="100">${loginedUser.u_id }</td>
		</tr>
		<tr>
			<td width="100"><b>비밀번호 : </b></td>
			<td width="100">${loginedUser.u_pass }</td>
		</tr>
		<tr>
			<td width="100"><b>이름 : </b></td>
			<td width="100">${loginedUser.u_name }</td>
		</tr>
		<tr>                          
			<td width="100"><b>직책 : </b></td>
			<td width="100">${loginedUser.u_position }</td>
		</tr>
		<tr>
			<td width="100"><b>점포 : </b></td>
			<td width="100">${loginedUser.u_store }</td>   
		</tr>
	</table>
	<br><br><br>
	<a href="${pageContext.request.contextPath}/modify.do"><input class="btn btn-success" type="button" value="수정"></a>
	<a href="${pageContext.request.contextPath}/delete.do?userId=${logined.u_id }"><input class="btn btn-danger" type="button" value="탈퇴"></a>
	<input class="btn btn-warning" type="button" value="뒤로가기" onclick="history.back(-1);">


</div>
</body>
</html>