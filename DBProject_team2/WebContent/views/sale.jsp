<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="/views/header/header.jsp"%>

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css"
	type="text/css" />
<title>페이지이름</title>

</head>
<body>

	<!-- Container ======================================================================================= -->
	<div class="cont_detail detail_product">
		<div class="inner_detail">

			<form action="searchByDate.do">
				<input name=date id="datepicker1" type="text"> <input
					class="btn btn-xs btn-default" type="submit" value="검색">
			</form>
			<ul class="list_product">
				<c:forEach var="sale" items="${saleList}">
					<li><a class="link_product"><span class="wrap_thumb">
								<img src="resources/img/${sale.p_id}.jpg" class="thumb_g" alt="">
						</span><strong class="tit_product">${sale.p_name}</strong> <em
							class="emph_price"> <span class="screen_out"></span> <span
								class="current_price">판매수량 : ${sale.sa_amount} </span> 판매 날짜 :
								${sale.sa_date}
						</em></a></li>
				</c:forEach>
			</ul>
			<input type="button" class="btn btn-default" value="뒤로가기"
				onclick="history.back(-1);">
		</div>
	</div>
</body>
<!-- // jQuery 기본 js파일 -->
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<!-- // jQuery UI 라이브러리 js파일 -->
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
<script>
	$(function() {
		$("#datepicker1").datepicker({
			dateFormat : 'yy-mm-dd'
		});
	});
</script>
</html>