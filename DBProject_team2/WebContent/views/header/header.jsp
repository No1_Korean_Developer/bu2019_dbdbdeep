﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/custom.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/custom2.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/custom3.css">

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid2">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapseds"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<div class="navbar-brand">
					<img src="resources/img/logo3.png"
						style="width: 200px; height: 30px;">
				</div>

			</div>


			<ul class="nav navbar-nav navbar-right">
				<li>
					<div class="nav navbar-nav navbar-left">
						<c:choose>
							<c:when test="${loginedUser eq null }">
								<li><a href="${pageContext.request.contextPath}/login.do">Login</a>
								</li>
								<li><a href="${pageContext.request.contextPath}/join.do">Join</a>
								</li>
							</c:when>
							<c:otherwise>
								<li
									style="line-height: 21px; padding-top: 8px; margin-right: 12px">
									<b>${loginedUser.u_name}</b> 님 반갑습니다.
								</li>
								<li
									style="line-height: 21px; padding-top: 8px; margin-right: 12px">
									접속 매장번호 :<b>${loginedUser.u_store}</b>
								</li>
								<li><a href="${pageContext.request.contextPath}/logout.do">Logout</a>
								</li>
								<li><a
									href="${pageContext.request.contextPath}/DetailUser.do">마이페이지</a>
								</li>
							</c:otherwise>
						</c:choose>
					</div>
				</li>
			</ul>
		</div>
	</nav>


	<form action="searchByName.do">
		매장별 재고 확인 : <select name="u_storeList"><option value=1
				<c:if test="${storeid == 1}">selected='selected'</c:if>>1.강남
				플래그십 스토어</option>
			<option value=2
				<c:if test="${storeid == 2}">selected='selected'</c:if>>2.홍대
				플래그십 스토어</option>
			<option value=3
				<c:if test="${storeid == 3}">selected='selected'</c:if>>3.부산
				플래그십 스토어</option>
			<option value=4
				<c:if test="${storeid == 4}">selected='selected'</c:if>>4.롯데
				영플라자 명동점</option>
			<option value=5
				<c:if test="${storeid == 5}">selected='selected'</c:if>>5.타임스퀘어점</option>
			<option value=6
				<c:if test="${storeid == 6}">selected='selected'</c:if>>6.롯데월드타워
				면세점</option>
			<option value=7
				<c:if test="${storeid == 7}">selected='selected'</c:if>>7.DDP
				동대문점</option>
			<option value=8
				<c:if test="${storeid == 8}">selected='selected'</c:if>>8.신라아이파크
				면세 용산점</option>
			<option value=9
				<c:if test="${storeid == 9}">selected='selected'</c:if>>9.신세계면세
				명동점</option>
			<option value=10
				<c:if test="${storeid == 10}">selected='selected'</c:if>>10.롯데월드몰
				잠실점</option>
			<option value=11
				<c:if test="${storeid == 11}">selected='selected'</c:if>>11.신세계백화점
				강남점</option>
			<option value=12
				<c:if test="${storeid == 12}">selected='selected'</c:if>>12.CGV
				여의도점</option>
			<option value=13
				<c:if test="${storeid == 13}">selected='selected'</c:if>>13.스타필드
				코엑스몰점</option>
			<option value=14
				<c:if test="${storeid == 14}">selected='selected'</c:if>>14.현대백화점
				신촌점</option>
		</select> 상품이름 검색 : <input name=p_nameList type="text"
			placeholder=" 이름을 입력하세요." value=""> <input
			class="btn btn-xs btn-default" type="submit" value="검색">
	</form>

	<form action="searchCustomer.do">
		<span>고객 전화번호 입력 : </span> <input name=customer type="text"
			placeholder="전화번호 입력" value="" maxlength=11> <input
			class="btn btn-xs btn-default" type="submit" value="검색">
	</form>

	<div>
		<c:choose>
			<c:when test="${empty customer}">
			</c:when>
			<c:otherwise>
				<div>
					<input type="hidden" name="c_phone" value="${customer.c_phone}">
					<b>${customer.c_name} 고객님</b> <a
						href="${pageContext.request.contextPath}/buyList.do">
						<button type="button" class="btn btn btn-warning">구매 내역</button>
					</a><a
						href="${pageContext.request.contextPath}/logout2.do">
						<button type="button" class="btn btn btn-default">취소</button>
					</a>
				</div>
			</c:otherwise>
		</c:choose>
	</div>

	<a href="${pageContext.request.contextPath}/list.do">
		<button type="button" class="btn btn btn-warning">상품 목록</button>
	</a>

	<a href="${pageContext.request.contextPath}/select.do">
		<button type="button" class="btn btn btn-warning">장바구니</button>
	</a>

	<a href="${pageContext.request.contextPath}/order.do">
		<button type="button" class="btn btn btn-warning">발주 내역</button>
	</a>

	<a href="${pageContext.request.contextPath}/sale.do">
		<button type="button" class="btn btn btn-warning">판매 내역</button>
	</a>

	<a href="${pageContext.request.contextPath}/regist.do">
		<button type="button" class="btn btn btn-warning">신규 고객</button>
	</a>

	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

</body>

</html>