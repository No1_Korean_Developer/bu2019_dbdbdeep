<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="/views/header/header.jsp"%>
<title>카카오 프렌즈샵</title>
<style>

</style>

</head>
<body>
<div align="center">

<br>
	<table>
		<tr>
		<form action="${pageContext.request.contextPath}/modify.do" method="POST">
		
			<td width="100"><b>ID : </b></td>
			<td width="100">${loginedUser.u_id }</td>
		</tr>
		<tr>
			<td width="100"><b>비밀번호 : </b></td>
			<td width="100"><input id="u_pass" name="u_pass" value="${loginedUser.u_pass }" class="form-control"
								type="password" ></td>
		</tr>
		<tr>
			<td width="100"><b>이름 : </b></td>
			<td width="100"><input id="u_name" name="u_name" value="${loginedUser.u_name }" class="form-control"
								type="text" ></td>
		</tr>
		<tr>
				<th>직책 :</th>
				<td><select name="u_position"><option>매니저</option><option>직원</option></select></td>
			</tr>
			<tr>
				<th>점포명 :</th> 
				<td><select name="u_store"><option value=1>강남 플래그십 스토어</option><option value=2>홍대 플래그십 스토어</option>
				<option value=3>부산 플래그십 스토어</option><option value=4>롯데 영플라자 명동점</option>
				<option value=5>타임스퀘어점</option><option value=6>롯데월드타워 면세점</option>
				<option value=7>DDP 동대문점</option><option value=8>신라아이파크 면세 용산점</option>
				<option value=9>신세계면세 명동점</option><option value=10>롯데월드몰 잠실점</option>
				<option value=11>신세계백화점 강남점</option><option value=12>CGV 여의도점</option>
				<option value=13>스타필드 코엑스몰점</option><option value=14>현대백화점 신촌점</option>
				</select></td>
			</tr>
	</table>
	<br><br><br>			
			</div>
			<center>
			<input class="btn btn-success" type="submit" value="수정"><input class="btn btn-warning" type="button" value="뒤로가기" onclick="history.back(-1);">

			</center>
	</div>
		</form>
</body>
</html>