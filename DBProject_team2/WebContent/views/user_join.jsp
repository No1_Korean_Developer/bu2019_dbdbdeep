<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html lang="ko">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<head>
<title>카카오 프렌즈샵</title>

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid2">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapseds"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<div class="navbar-brand">
					<img src="resources/img/logo3.png" style="width: 200px; height:30px;">
				</div>

			</div>


			<ul class="nav navbar-nav navbar-right">
				<li>
					<div class="nav navbar-nav navbar-left">
						<c:choose>
							<c:when test="${loginedUser eq null }">
								<li><a
									href="${pageContext.request.contextPath}/login.do">Login</a>
								</li>
								<li><a
									href="${pageContext.request.contextPath}/join.do">Join</a>
								</li>
							</c:when>
							<c:otherwise>
								<li
									style="line-height: 21px; padding-top: 8px; margin-right: 12px">
									<b>${loginedUser.u_name}</b> 님 반갑습니다.
								</li>
								<li
									style="line-height: 21px; padding-top: 8px; margin-right: 12px">
									접속 매장번호 :<b>${loginedUser.u_store}</b>
								</li>
								<li><a
									href="${pageContext.request.contextPath}/logout.do">Logout</a>
								</li>
								<li><a
									href="${pageContext.request.contextPath}/DetailUser.do">마이페이지</a>
								</li>
							</c:otherwise>
						</c:choose>
					</div>
				</li>
			</ul>
		</div>
	</nav>

<div align="center">
<form action="${pageContext.request.contextPath}/join.do" method="post">
		<table class="table">
			<tr>
				<th>ID :</th>
				<td><input type="text"  name="u_id" maxlength=12 class="form-control" type="text" value="" placeholder="ID"></td>
			</tr>
			<tr>
				<th>비밀번호 :</th>
				<td><input type="password" name="u_pass" maxlength=20 class="form-control" type="password" value="" placeholder="비밀번호"></td>
			</tr>
			<tr>
				<th>이름 :</th>
				<td><input type="text" name="u_name" maxlength=10 class="form-control" type="text" value="" placeholder="이름"></td>
			</tr>
			<tr>
				<th>직책 :</th>
				<td><select name="u_position"><option>매니저</option><option>직원</option></select></td>
			</tr>
			<tr>
				<th>점포명 :</th> 
				<td><select name="u_store"><option value=1>강남 플래그십 스토어</option><option value=2>홍대 플래그십 스토어</option>
				<option value=3>부산 플래그십 스토어</option><option value=4>롯데 영플라자 명동점</option>
				<option value=5>타임스퀘어점</option><option value=6>롯데월드타워 면세점</option>
				<option value=7>DDP 동대문점</option><option value=8>신라아이파크 면세 용산점</option>
				<option value=9>신세계면세 명동점</option><option value=10>롯데월드몰 잠실점</option>
				<option value=11>신세계백화점 강남점</option><option value=12>CGV 여의도점</option>
				<option value=13>스타필드 코엑스몰점</option><option value=14>현대백화점 신촌점</option>
				</select></td>
			</tr>
		</table><br>
		<div align="center"><input class="btn btn-danger" type="reset" value="취소"> <input class="btn btn-success" type="submit" value="회원가입"></div>
		<input class="btn btn-warning" type="button" value="뒤로가기" onclick="history.back(-1);">

	</form>
</div>
</body>
</html>