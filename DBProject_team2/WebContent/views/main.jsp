<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="ko">
<head>
<%@ include file="/views/header/header.jsp"%>
<title>페이지이름</title>
</head>
<meta charset="utf-8">

<div class="cont_product">
	<form name='myForm' method='post'>

		<ul class="list_product">
			<c:forEach var="product" items="${productList}">
				<li><a class="link_product"><span class="wrap_thumb">
							<img src="resources/img/${product.p_id}.jpg" class="thumb_g"
							alt="">
					</span><strong class="tit_product">${product.p_name}</strong> <em
						class="emph_price"> <span class="screen_out"></span><span
							class="current_price">금액 ${product.p_price}원 <span
								class="usd_price">재고 ${product.p_stock}개</span></span>
					</em></a><span><input type="checkbox" id="checkBoxId_${product.p_id}"
						class="chk_g" name="p_ids" value="${product.p_id}"></span> <span><input
						type="button" value=" - " class="bt_down_${product.p_id}"
						style="text-align: center;" /> <input type="text"
						id="stock_${product.p_id}" name="p_stocks" value="0"
						style="text-align: center; width: 30px; height: 30px;"> <input
						type="button" value=" + " class="bt_up_${product.p_id}"
						style="text-align: center;" /></span></li>

			</c:forEach>
		</ul>

		<ul class="btn-group pagination">
			<c:if test="${pageMaker.prev }">
				<li><a
					href='<c:url value="/list.do?page=${pageMaker.startPage-1 }"/>'>
						<< </a></li>
			</c:if>
			<c:forEach begin="${pageMaker.startPage }"
				end="${pageMaker.endPage }" var="idx">
				<li><a href='<c:url value="/list.do?page=${idx }"/>'><i
						class="fa">${idx }</i></a></li>
			</c:forEach>
			<c:if test="${pageMaker.next && pageMaker.endPage >0 }">
				<li><a
					href='<c:url value="/list.do?page=${pageMaker.endPage+1 }"/>'>>></a>
				</li>
			</c:if>
		</ul>

		<div class="text-right">
			<input type="button" onClick='mySubmit(1)' class="btn btn-default"
				value='담기'> <input type="button" onClick='mySubmit(2)'
				class="btn btn-default" value='재고추가'> <input type="button"
				onClick='mySubmit(3)' class="btn btn-default" value='판매'>
		</div>
	</form>
</div>


</body>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
$(document).ready(function() {

	$("input[name=p_ids]").each(function(idx) {

		var value = $(this).val();
		var eqValue = $("input[name=p_ids]:eq(" + idx + ")").val();

		$("#stock_" + (value)).attr("disabled", true);
		$('.bt_up_'+ (value)).attr("disabled", true);
		$('.bt_down_'+ (value)).attr("disabled", true);
		
		$("#checkBoxId_" + (value)).change(function() {
			if ($("#checkBoxId_" + (value)).is(":checked")) {

				$("#stock_" + (value)).attr("disabled", false);
				$("#stock_" + (value)).val("0");
				
				$('.bt_up_'+ (value)).attr("disabled", false);
				$('.bt_down_'+ (value)).attr("disabled", false);

			} else {

				$("#stock_" + (value)).attr("disabled", true);
				
				$('.bt_up_'+ (value)).attr("disabled", true);
				$('.bt_down_'+ (value)).attr("disabled", true);
			}
		});
		
		$(function(){ 
			  $('.bt_up_'+ (value)).click(function(){ 
			    var n = $('.bt_up_'+ (value)).index(this);
			    var num = $("#stock_" + (value) + ":eq("+n+")").val();
			    num = $("#stock_" + (value) + ":eq("+n+")").val(num*1+1); 
			  });
			  $('.bt_down_'+ (value)).click(function(){ 
			    var n = $('.bt_down_'+ (value)).index(this);
			    var num = $("#stock_" + (value) + ":eq("+n+")").val();
			    num = $("#stock_" + (value) + ":eq("+n+")").val(num*1-1); 
			  });
			}) 
		
		
	});
});
function mySubmit(index) {
	if (index == 1) {
		document.myForm.action = "${pageContext.request.contextPath}/list.do";
	}
	if (index == 2) {
		document.myForm.action = "${pageContext.request.contextPath}/list2.do";
	}
	if (index == 3) {
		document.myForm.action = "${pageContext.request.contextPath}/list3.do";
	}
	document.myForm.submit();
}
</script>

</html>