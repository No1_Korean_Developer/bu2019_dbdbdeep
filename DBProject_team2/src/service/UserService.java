package service;

import domain.User;

public interface UserService {
	
	boolean RegistUser(User user);
	void DeleteUser(String u_id);
	User login(String u_id);
	void modify(User user);
	
}
