package service;

import java.sql.Date;
import java.util.List;

import domain.Product;
import domain.User;
import page.Criteria;

public interface MainService  {
	
	List<Product> selectAll(User user);

	List<Product> readAll(int s_id, Criteria cri);

	void sendProducts(Product product);

	List<Product> SearchByName(String name, int s_id, Criteria cri);

	void sendSeletes(Product product);

	void addStocks(Product product);

	void deleteSeletions(Product product);

	List<Product> orderAll(int u_store);
	
	List<Product> saleAll(int u_store);

	void removeStocks(Product product);

	List<Product> SearchByDate(Date sa_date, int s_id);

	void saleProducts(Product product);
	
}
