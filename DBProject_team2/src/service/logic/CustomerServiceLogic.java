package service.logic;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.Customer;
import domain.Product;
import service.CustomerService;
import store.CustomerStore;

@Service
public class CustomerServiceLogic implements CustomerService{
	
	@Autowired
	private CustomerStore store;
	
	@Override
	public void ResistCustomer(Customer customer) {
		store.InsertCustomer(customer);
	}
	@Override
	public void DeleteCustomer(String c_id) {
		store.DeleteCustomer(c_id);
	}
	@Override
	public void BuyCustomer(Customer customer, Product product) {
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		store.BuyCustomer(customer, product, today);
		
	}
	@Override
	public Customer searchByPhone(int phone) {
		// TODO Auto-generated method stub
		return store.searchCustomer(phone);
	}
	@Override
	public List<Product> buyReadAll(int c_phone) {
		// TODO Auto-generated method stub
		return store.buyListProducts(c_phone);
	}
	

}
