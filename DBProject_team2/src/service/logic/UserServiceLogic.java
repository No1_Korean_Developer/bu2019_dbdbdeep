package service.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.User;
import service.UserService;
import store.UserStore;

@Service
public class UserServiceLogic implements UserService {

	@Autowired
	private UserStore store;

	@Override
	public boolean RegistUser(User user) {
		
		return store.InsertUser(user);
	}

	@Override
	public void DeleteUser(String u_id) {
		store.DeleteUser(u_id);
		
	}

	@Override
	public User login(String u_id) {
		
		return store.findUserId(u_id);
	}

	@Override
	public void modify(User user) {

		store.modify(user);
	
	}

}
