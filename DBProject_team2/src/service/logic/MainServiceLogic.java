package service.logic;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.Product;
import domain.User;
import page.Criteria;
import service.MainService;
import store.MainStore;

@Service
public class MainServiceLogic implements MainService {

	@Autowired
	private MainStore store;

	@Override
	public List<Product> selectAll(User user) {
		// TODO Auto-generated method stub
		return store.readBySelect(user);
	}
	@Override
	public List<Product> readAll(int s_id, Criteria cri) {
		return store.readAll(s_id, cri);
	}
	@Override
	public List<Product> orderAll(int s_id) {
		return store.readByOrder(s_id);
	}
	@Override
	public void sendProducts(Product product) {
		// TODO Auto-generated method stub
		
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		store.sendProduct(product, today);
	}	
	@Override
	public List<Product> SearchByName(String name, int s_id, Criteria cri) {
		// TODO Auto-generated method stub
		return store.readByName(name, s_id, cri);
	}
	@Override
	public void sendSeletes(Product product) {
		// TODO Auto-generated method stub
		store.sendSelete(product);
	}
	@Override
	public void addStocks(Product product) {
		// TODO Auto-generated method stub
		store.addStock(product);
	}
	@Override
	public void deleteSeletions(Product product) {
		// TODO Auto-generated method stub
		store.deleteSeletion(product);
	}
	
	@Override
	public List<Product> saleAll(int s_id) {
		return store.readBySale(s_id);
	}
	@Override
	public void removeStocks(Product product) {
		// TODO Auto-generated method stub
		store.deleteStock(product);
	}
	@Override
	public List<Product> SearchByDate(Date sa_date, int s_id) {
		return store.readByDate(sa_date, s_id);
	}
	@Override
	public void saleProducts(Product product) {
		// TODO Auto-generated method stub
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		store.salePorduct(product, today);
	}


}
