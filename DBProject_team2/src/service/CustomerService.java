package service;

import java.util.List;

import domain.Customer;
import domain.Product;

public interface CustomerService {
	
	void ResistCustomer(Customer customer);
	void DeleteCustomer(String customer);
	void BuyCustomer(Customer customer, Product product);
	Customer searchByPhone(int phone);
	List<Product> buyReadAll(int c_phone);
	

}
