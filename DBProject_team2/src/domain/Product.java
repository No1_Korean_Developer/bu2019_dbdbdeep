package domain;

import java.sql.Date;

public class Product {
	
	private int p_id; 
	private String p_name; 
	private int p_price;
	
	private int s_id;
	private int p_stock; 
	
	private String u_id;
	private int se_id;
	
	private int[] p_stocks;
	private int[] p_ids;
	
	private Date o_date;
	
	private String u_name;
	private String s_addr;
	
	private int sa_amount; // 판매량 
	private Date sa_date;	//판매날짜
	
	private int c_phone;

	

	
	
	public int getP_id() {
		return p_id;
	}
	public void setP_id(int p_id) {
		this.p_id = p_id;
	}
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	public int getP_price() {
		return p_price;
	}
	public void setP_price(int p_price) {
		this.p_price = p_price;
	}
	public int getP_stock() {
		return p_stock;
	}
	public void setP_stock(int p_stock) {
		this.p_stock = p_stock;
	}
	public String getU_id() {
		return u_id;
	}
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}
	public int getSe_id() {
		return se_id;
	}
	public void setSe_id(int se_id) {
		this.se_id = se_id;
	}
	public int[] getP_stocks() {
		return p_stocks;
	}
	public void setP_stocks(int[] p_stocks) {
		this.p_stocks = p_stocks;
	}
	public int[] getP_ids() {
		return p_ids;
	}
	public void setP_ids(int[] p_ids) {
		this.p_ids = p_ids;
	}
	public int getS_id() {
		return s_id;
	}
	public void setS_id(int s_id) {
		this.s_id = s_id;
	}
	public Date getO_date() {
		return o_date;
	}
	public void setO_date(Date o_date) {
		this.o_date = o_date;
	}
	public String getU_name() {
		return u_name;
	}
	public void setU_name(String u_name) {
		this.u_name = u_name;
	}
	public String getS_addr() {
		return s_addr;
	}
	public void setS_addr(String s_addr) {
		this.s_addr = s_addr;
	}
	public int getSa_amount() {
		return sa_amount;
	}
	public void setSa_amount(int sa_amount) {
		this.sa_amount = sa_amount;
	}
	public Date getSa_date() {
		return sa_date;
	}
	public void setSa_date(Date sa_date) {
		this.sa_date = sa_date;
	}
	public int getC_phone() {
		return c_phone;
	}
	public void setC_phone(int c_phone) {
		this.c_phone = c_phone;
	}
}
