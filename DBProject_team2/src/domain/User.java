package domain;

public class User {
	private String u_id; //user id
	private String u_pass; //user password
	private String u_name; // user name
	private String u_position; //user 직책
	private int u_store;		// user 매장
	
	public String getU_id() {
		return u_id;
	}
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}
	public String getU_pass() {
		return u_pass;
	}
	public void setU_pass(String u_pass) {
		this.u_pass = u_pass;
	}
	public String getU_name() {
		return u_name;
	}
	public void setU_name(String u_name) {
		this.u_name = u_name;
	}
	public String getU_position() {
		return u_position;
	}
	public void setU_position(String u_position) {
		this.u_position = u_position;
	}
	public int getU_store() {
		return u_store;
	}
	public void setU_store(int u_store) {
		this.u_store = u_store;
	}
	

}
