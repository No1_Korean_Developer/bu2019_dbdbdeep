package domain;

public class Customer {
	
	private int c_phone;
	private int c_pass;
	private int c_birth;
	private String c_name;
	
	
	public int getC_phone() {
		return c_phone;
	}
	public void setC_phone(int c_phone) {
		this.c_phone = c_phone;
	}
	public int getC_pass() {
		return c_pass;
	}
	public void setC_pass(int c_pass) {
		this.c_pass = c_pass;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public int getC_birth() {
		return c_birth;
	}
	public void setC_birth(int c_birth) {
		this.c_birth = c_birth;
	}

}
