package store;

import java.sql.Date;
import java.util.List;

import domain.Customer;
import domain.Product;

public interface CustomerStore {
	
	void InsertCustomer(Customer customer);
	void DeleteCustomer(String c_id);
	//void BuyCustomer(Customer customer);
	//void BuyCustomer(String c_id, String p_id, String p_name, String p_qty);
	void BuyCustomer(Customer customer, Product product, Date today);
	Customer searchCustomer(int phone);
	List<Product> buyListProducts(int c_phone);

}
