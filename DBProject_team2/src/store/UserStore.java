package store;

import domain.User;

public interface UserStore {

	boolean InsertUser(User user);
	void DeleteUser(String u_id);
	User findUserId(String u_id);
	void modify(User user);
}
