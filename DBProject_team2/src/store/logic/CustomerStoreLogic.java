package store.logic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Customer;
import domain.Product;
import store.CustomerStore;
import store.factory.ConnectionFactory;
import store.factory.JdbcUtils;

@Repository
public class CustomerStoreLogic implements CustomerStore {

	private ConnectionFactory connectionFactory;

	public CustomerStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public void InsertCustomer(Customer customer) {

		Connection connection = null;
		PreparedStatement psmt = null;

		try {
			connection = connectionFactory.createConnection();

			psmt = connection
					.prepareStatement("INSERT INTO S_CUSTOMER (c_phone, c_pass, c_birth, c_name) VALUES (?,?,?,?)");

			psmt.setInt(1, customer.getC_phone());
			psmt.setInt(2, customer.getC_pass());
			psmt.setInt(3, customer.getC_birth());
			psmt.setString(4, customer.getC_name());

			psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

	}

	@Override
	public void DeleteCustomer(String c_id) {
		String sql = "DELETE FROM S_CUSTOMER WHERE C_ID = ?";

		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, c_id);
			psmt.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public void BuyCustomer(Customer customer, Product product, Date today) {
		Connection conn = null;
		PreparedStatement psmt = null;

		ResultSet rs = null;
		Date dateTemp = null;

		String sql = "SELECT pu_date FROM s_purchase WHERE p_id = ? and pu_date = ?";
		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, product.getP_id());
			psmt.setDate(2, today);
			rs = psmt.executeQuery();
			while (rs.next()) {
				dateTemp = rs.getDate(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement("UPDATE s_purchase SET pu_amount = pu_amount + ? WHERE p_id = ? and pu_date = ?");
			psmt.setInt(1, product.getP_stock());
			psmt.setInt(2, product.getP_id());
			psmt.setDate(3, today);
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		
		if (dateTemp == null) {
			try {
				conn = connectionFactory.createConnection();

				psmt = conn.prepareStatement("INSERT INTO s_purchase (c_phone, p_id, pu_amount, pu_date) VALUES (?,?,?,?)");
				psmt.setInt(1, customer.getC_phone());
				psmt.setInt(2, product.getP_id());
				psmt.setInt(3, product.getP_stock());
				psmt.setDate(4, today);
				psmt.executeUpdate();

			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				JdbcUtils.close(psmt, conn);
			}
		}
		
	}


	@Override
	public Customer searchCustomer(int phone) {
		String sql = "select c_name, c_phone from s_customer where c_phone = ? ";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Customer customer = new Customer();
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, phone);
			rs = psmt.executeQuery();
			while (rs.next()) {
				customer.setC_name(rs.getString(1));
				customer.setC_phone(rs.getInt(2));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return customer;
	}

	@Override
	public List<Product> buyListProducts(int c_phone) {
		String sql = "select c_phone, s_purchase.p_id, p_name, pu_amount, pu_date from s_purchase inner join s_product on s_purchase.p_id = s_product.p_id where c_phone = ?";

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Product> buylist = new ArrayList<Product>();

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(sql.toString());
			psmt.setInt(1, c_phone);
			rs = psmt.executeQuery();

			while (rs.next()) {
				Product product = new Product();
				product.setC_phone(rs.getInt(1));
				product.setP_id(rs.getInt(2));
				product.setP_name(rs.getString(3));
				product.setSa_amount(rs.getInt(4));
				product.setSa_date(rs.getDate(5));
				buylist.add(product);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, connection);
		}
		return buylist;
	}

}
