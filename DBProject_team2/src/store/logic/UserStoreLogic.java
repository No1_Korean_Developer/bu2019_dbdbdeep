package store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import domain.User;
import store.UserStore;
import store.factory.ConnectionFactory;
import store.factory.JdbcUtils;

@Repository
public class UserStoreLogic implements UserStore {

	private ConnectionFactory connectionFactory;

	public UserStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public boolean InsertUser(User user) {

		Connection connection = null;
		PreparedStatement psmt = null;
		int insertedCount = 0;
		try {
			connection = connectionFactory.createConnection();

			psmt = connection.prepareStatement(
					"INSERT INTO S_USER(U_ID, U_PASS, U_NAME, U_POSITION, U_STORE) VALUES (?,?,?,?,?)");

			psmt.setString(1, user.getU_id());
			psmt.setString(2, user.getU_pass());
			psmt.setString(3, user.getU_name());
			psmt.setString(4, user.getU_position());
			psmt.setInt(5, user.getU_store());

			insertedCount = psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

		return insertedCount > 0;
	}

	@Override
	public void DeleteUser(String userId) {
		String sql = "DELETE FROM S_USER WHERE U_ID = ?";

		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, userId);
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public User findUserId(String userId) {
		String sql = "SELECT U_ID , U_NAME, U_PASS, U_POSITION, U_STORE FROM S_USER WHERE U_ID= ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		User useridcheak = null;
		try {
			useridcheak = new User();
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, userId);
			rs = psmt.executeQuery();

			if (rs.next()) {
				useridcheak.setU_id(rs.getString("U_ID"));
				useridcheak.setU_name(rs.getString("U_NAME"));
				useridcheak.setU_pass(rs.getString("U_PASS"));
				useridcheak.setU_position(rs.getString("U_POSITION"));
				useridcheak.setU_store(rs.getInt("U_STORE"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return useridcheak;
	}

	@Override
	public void modify(User user) {
		String sql = "UPDATE S_USER SET U_PASS=?, U_NAME=?, U_POSITION=?, U_STORE=? WHERE U_ID = ?";
		Connection conn = null;
		PreparedStatement psmt = null;

		System.out.println(user.getU_id());
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, user.getU_pass());
			psmt.setString(2, user.getU_name());
			psmt.setString(3, user.getU_position());
			psmt.setInt(4, user.getU_store());
			psmt.setString(5, user.getU_id());
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}

	}

}
