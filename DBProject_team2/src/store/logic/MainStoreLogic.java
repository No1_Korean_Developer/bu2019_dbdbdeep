package store.logic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Product;
import domain.User;
import page.Criteria;
import store.MainStore;
import store.factory.ConnectionFactory;
import store.factory.JdbcUtils;

@Repository
public class MainStoreLogic implements MainStore {

	private ConnectionFactory connectionFactory;

	public MainStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public List<Product> readBySelect(User user) {

		String sql = "SELECT se_id, s_select.u_id, s_stock.p_id, p_name, p_price, p_stock from s_select inner join s_product on s_select.p_id = s_product.p_id inner join s_stock on s_product.p_id = s_stock.p_id where u_id = ? and s_stock.s_id = ? ";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Product> products = new ArrayList<Product>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, user.getU_id());
			psmt.setInt(2, user.getU_store());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setSe_id(rs.getInt(1));
				product.setU_id(rs.getString(2));
				product.setP_id(rs.getInt(3));
				product.setP_name(rs.getString(4));
				product.setP_price(rs.getInt(5));
				product.setP_stock(rs.getInt(6));
				products.add(product);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return products;

	}

	@Override
	public List<Product> readAll(int s_id, Criteria cri) {
		String sql = "SELECT s_stock.p_id, p_name, p_price, p_stock, s_id from s_product inner join s_stock on s_stock.p_id = s_product.p_id where s_id = ?"
				+ " LIMIT " + cri.getPageStart() + "," + cri.getPerPageNum();

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Product> list = new ArrayList<Product>();

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(sql.toString());
			psmt.setInt(1, s_id);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setP_id(rs.getInt(1));
				product.setP_name(rs.getString(2));
				product.setP_price(rs.getInt(3));
				product.setP_stock(rs.getInt(4));
				product.setS_id(rs.getInt(5));
				list.add(product);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			JdbcUtils.close(rs, psmt, connection);
		}
		return list;
	}

	@Override
	public void sendProduct(Product product, Date today) {
		Connection conn = null;
		PreparedStatement psmt = null;
		PreparedStatement psmt2 = null;

		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement("INSERT INTO s_order (u_id, p_id, s_id, o_stock, o_date) VALUES (?,?,?,?,?)");

			psmt.setString(1, product.getU_id());
			psmt.setInt(2, product.getP_id());
			psmt.setInt(3, product.getS_id());
			psmt.setInt(4, product.getP_stock());
			psmt.setDate(5, today);

			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		try {
			conn = connectionFactory.createConnection();

			psmt2 = conn.prepareStatement("delete from s_select where u_id = ? and p_id = ?");

			psmt2.setString(1, product.getU_id());
			psmt2.setInt(2, product.getP_id());

			psmt2.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt2, conn);
		}
	}

	@Override
	public List<Product> readByName(String name, int s_id, Criteria cri) {
		String sql = "SELECT s_stock.p_id, p_name, p_price, p_stock from s_product inner join s_stock on s_stock.p_id = s_product.p_id where p_name Like ? and s_id = ?"
				+ " LIMIT " + cri.getPageStart() + "," + cri.getPerPageNum();

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Product> products = new ArrayList<Product>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, "%" + name + "%");
			psmt.setInt(2, s_id);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setP_id(rs.getInt(1));
				product.setP_name(rs.getString(2));
				product.setP_price(rs.getInt(3));
				product.setP_stock(rs.getInt(4));
				products.add(product);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return products;

	}

	@Override
	public void sendSelete(Product product) {
		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("INSERT INTO s_select (u_id, p_id, s_id) VALUES (?,?,?)");

			psmt.setString(1, product.getU_id());
			psmt.setInt(2, product.getP_id());
			psmt.setInt(3, product.getS_id());

			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public void addStock(Product product) {
		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("UPDATE s_stock SET p_stock = p_stock + ? WHERE s_id = ? and p_id = ?");

			System.out.println(product.getP_stock());
			System.out.println(product.getS_id());
			System.out.println(product.getP_id());

			psmt.setInt(1, product.getP_stock());
			psmt.setInt(2, product.getS_id());
			psmt.setInt(3, product.getP_id());

			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public void deleteSeletion(Product product) {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("delete from s_select where u_id = ? and p_id = ?");

			psmt.setString(1, product.getU_id());
			psmt.setInt(2, product.getP_id());

			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public List<Product> readByOrder(int s_id) {
		String sql = "select s_product.p_id, u_name, s_addr, p_name, o_stock, o_date from s_order inner join s_product on s_order.p_id = s_product.p_id inner join s_user on s_order.u_id = s_user.u_id inner join s_store on s_order.s_id = s_store.s_id where s_order.s_id = ?";

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Product> lists = new ArrayList<Product>();

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(sql.toString());
			psmt.setInt(1, s_id);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setP_id(rs.getInt(1));
				product.setU_name(rs.getString(2));
				product.setS_addr(rs.getString(3));
				product.setP_name(rs.getString(4));
				product.setP_stock(rs.getInt(5));
				product.setO_date(rs.getDate(6));
				lists.add(product);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, connection);
		}
		return lists;

	}

	@Override
	public List<Product> readBySale(int s_id) {
		String sql = "select s_sale.p_id, u_name, p_name, sa_amount, sa_date from s_sale inner join s_product on s_sale.p_id = s_product.p_id inner join s_user on s_sale.u_id = s_user.u_id where u_store = ? order by sa_date desc";

		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Product> salist = new ArrayList<Product>();

		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(sql.toString());
			psmt.setInt(1, s_id);
			rs = psmt.executeQuery();

			while (rs.next()) {
				Product product = new Product();
				product.setP_id(rs.getInt(1));
				product.setU_name(rs.getString(2));
				product.setP_name(rs.getString(3));
				product.setSa_amount(rs.getInt(4));
				product.setSa_date(rs.getDate(5));
				salist.add(product);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, connection);
		}
		return salist;
	}

	@Override
	public void deleteStock(Product product) {
		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("UPDATE s_stock SET p_stock = p_stock - ? WHERE s_id = ? and p_id = ?");

			System.out.println(product.getP_stock());
			System.out.println(product.getS_id());
			System.out.println(product.getP_id());

			psmt.setInt(1, product.getP_stock());
			psmt.setInt(2, product.getS_id());
			psmt.setInt(3, product.getP_id());

			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}
	
	@Override
	public List<Product> readByDate(Date sa_date, int s_id) {
		String sql = "select p_name, sa_amount, sa_date, s_sale.p_id from s_sale inner join s_product on s_sale.p_id = s_product.p_id inner join s_user on s_sale.u_id = s_user.u_id where sa_date= ? and u_store = ?";
		
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Product> productsa = new ArrayList<Product>();
		
		try {
			connection = connectionFactory.createConnection();
			psmt = connection.prepareStatement(sql.toString());
			psmt.setDate(1, sa_date);
			psmt.setInt(2, s_id);
			rs = psmt.executeQuery();
			
			while(rs.next()) {
				Product product = new Product();
				product.setP_name(rs.getString(1));
				product.setSa_amount(rs.getInt(2));
				product.setSa_date(rs.getDate(3));
				product.setP_id(rs.getInt(4));
				productsa.add(product);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, connection);
		}
		return productsa;
	}

	@Override
	public void salePorduct(Product product, Date today) {
		Connection conn = null;
		PreparedStatement psmt = null;

		ResultSet rs = null;
		Date dateTemp = null;

		String sql = "SELECT sa_date FROM s_sale WHERE p_id = ? and sa_date = ?";
		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, product.getP_id());
			psmt.setDate(2, today);
			rs = psmt.executeQuery();
			while (rs.next()) {
				dateTemp = rs.getDate(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement("UPDATE s_sale SET sa_amount = sa_amount + ? WHERE p_id = ? and sa_date = ?");
			psmt.setInt(1, product.getP_stock());
			psmt.setInt(2, product.getP_id());
			psmt.setDate(3, today);
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		
		if (dateTemp == null) {
			try {
				conn = connectionFactory.createConnection();

				psmt = conn.prepareStatement("INSERT INTO s_sale (p_id, u_id, sa_amount, sa_date) VALUES (?,?,?,?)");
				psmt.setInt(1, product.getP_id());
				psmt.setString(2, product.getU_id());
				psmt.setInt(3, product.getP_stock());
				psmt.setDate(4, today);
				psmt.executeUpdate();

			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				JdbcUtils.close(psmt, conn);
			}
		}
		
	}


}
