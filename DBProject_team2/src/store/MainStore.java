package store;

import java.sql.Date;
import java.util.List;

import domain.Product;
import domain.User;
import page.Criteria;

public interface MainStore {

	List<Product> readBySelect(User user);

	List<Product> readAll(int s_id, Criteria cri);

	void sendProduct(Product product, Date today);

	List<Product> readByName(String name, int s_id, Criteria cri);

	void sendSelete(Product product);

	void addStock(Product product);

	void deleteSeletion(Product product);

	List<Product> readByOrder(int s_id);

	List<Product> readBySale(int s_id);

	void deleteStock(Product product);

	List<Product> readByDate(Date sa_date, int s_id);

	void salePorduct(Product product, Date today);
}
