package controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Customer;
import domain.Product;
import domain.User;
import service.CustomerService;

@Controller
public class CustomerController {
	
	@Autowired
	private CustomerService service;
	
	@RequestMapping(value = "regist.do", method = RequestMethod.GET )
	public String Regist() {
	
		return "customer_join";
	}
	
	@RequestMapping(value = "regist.do", method = RequestMethod.POST )
	public String Regist(Customer customer) {
	
		service.ResistCustomer(customer);
		return "redirect:/list.do";
	}
	
	@RequestMapping(value = "buyList.do")
	public ModelAndView SaleProducts(HttpSession session, Product product) {

		Customer customer = (Customer) session.getAttribute("customerI");

		List<Product> buyList = service.buyReadAll(customer.getC_phone());
		ModelAndView modelAndView = new ModelAndView("buylist");

		modelAndView.addObject("customer", customer);
		modelAndView.addObject("buyList", buyList);

		return modelAndView;
	}
	
	
}
