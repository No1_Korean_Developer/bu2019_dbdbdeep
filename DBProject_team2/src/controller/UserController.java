package controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.User;
import service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService service;
	
	@RequestMapping(value = "join.do", method = RequestMethod.GET )
	public String Join() {
		
		return "user_join";
	}
	
	@RequestMapping(value = "join.do", method = RequestMethod.POST )
	public String Join(User user) {
		service.RegistUser(user);
		return "login";
	}
	
	@RequestMapping("delete.do")
	public String Delete(HttpSession session) {
		User user = (User)session.getAttribute("loginedUser");
		String u_id = user.getU_id();
		service.DeleteUser(u_id);
		session.invalidate();
		return "DeleteUserM";
	}

	@RequestMapping(value = "login.do", method = RequestMethod.GET )
	public String Login() {
		
		return "login";
	}
	
	@RequestMapping(value = "login.do", method = RequestMethod.POST )
	public String Login(HttpSession session, User user, Model model) {
		User user1 = service.login(user.getU_id());

		if (user1.getU_id() == null || !user.getU_pass().equals(user1.getU_pass())) {
//				model.addAttribute("msg", "아이디가 맞지 않습니다."); 
//				model.addAttribute("url", "login");
			return "loginError";
		}

		if (user.getU_pass().equals(user1.getU_pass())) {
			session.setAttribute("loginedUser", user1);
		}
		return "redirect:/list.do";
	}

	@RequestMapping("logout.do")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:login.do";
	}
	
		@RequestMapping(value = "modify.do", method = RequestMethod.GET)
		public String modify(Model model, HttpSession session, User user) {
//			User user = (User) session.setAttribute("loginedUser",user);
//			User user = new User();
//			user = (User)session.getAttribute("user");
//			session.setAttribute("user", user);//세션 받아오기
//			model.addAttribute("user", "loginedUser");//모델에 추가
			return "ModifyUser";
		}
		
		@RequestMapping(value = "modify.do", method = RequestMethod.POST)
		public String modify(HttpSession session, User user, Model model) {
			
			User user1 = (User) session.getAttribute("loginedUser");
			user.setU_id(user1.getU_id());
			service.modify(user);
			session.setAttribute("loginedUser",user);
				
				return "DetailUser";
		}
		@RequestMapping(value = "DetailUser.do", method = RequestMethod.GET)
		public void detail(ModelAndView modelAndView, HttpSession session) {
			
			User user1 = (User) session.getAttribute("loginedUser");

			service.login(user1.getU_id());
				
				
		}

}
