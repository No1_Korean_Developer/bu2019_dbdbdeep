package controller;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Customer;
import domain.Product;
import domain.User;
import page.Criteria;
import page.PageMaker;
import service.CustomerService;
import service.MainService;

@Controller
public class MainController {

	@Autowired
	private MainService service;

	@Autowired
	private CustomerService cservice;

	@RequestMapping(value = "list.do", method = RequestMethod.GET)
	public ModelAndView list(Criteria cri, HttpSession session) {

		User user1 = (User) session.getAttribute("loginedUser");
		Customer customer = (Customer) session.getAttribute("customerI");

		int s_id = user1.getU_store();

		ModelAndView modelAndView = new ModelAndView("main");
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		pageMaker.setTotalCount(159);

		List<Product> list = service.readAll(s_id, cri);
		
		modelAndView.addObject("customer", customer);
		modelAndView.addObject("productList", list);
		modelAndView.addObject("storeNum", s_id);
		modelAndView.addObject("pageMaker", pageMaker);

		return modelAndView;

	}
	
	@RequestMapping("logout2.do")
	public ModelAndView logout2(Criteria cri, HttpSession session) {
		session.setAttribute("customerI", null);
		User user1 = (User) session.getAttribute("loginedUser");
		Customer customer = (Customer) session.getAttribute("customerI");

		int s_id = user1.getU_store();

		ModelAndView modelAndView = new ModelAndView("main");
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		pageMaker.setTotalCount(159);

		List<Product> list = service.readAll(s_id, cri);
		
		modelAndView.addObject("customer", customer);
		modelAndView.addObject("productList", list);
		modelAndView.addObject("storeNum", s_id);
		modelAndView.addObject("pageMaker", pageMaker);

		return modelAndView;
	}
	
	@RequestMapping(value = "list.do", method = RequestMethod.POST)
	public String SendSeletes(Product product, int[] p_ids, HttpSession session) {

		User user = (User) session.getAttribute("loginedUser");

		for (int p_id : p_ids) {

			product.setU_id(user.getU_id());
			product.setP_id(p_id);
			product.setS_id(user.getU_store());

			service.sendSeletes(product);

		}
		return "redirect:/select.do";
	}

	@RequestMapping(value = "list2.do", method = RequestMethod.POST)
	public String addStock(Product product, int[] p_stocks, int[] p_ids, HttpSession session) {

		User user = (User) session.getAttribute("loginedUser");

		int i = p_ids.length;

		for (int j = 0; j < i; j++) {

			int p_stock = p_stocks[j];
			int p_id = p_ids[j];

			product.setS_id(user.getU_store());
			product.setP_id(p_id);
			product.setP_stock(p_stock);

			service.addStocks(product);

		}
		return "redirect:/list.do";
	}

	@RequestMapping(value = "list3.do", method = RequestMethod.POST)
	public String saleStocks(Product product, int[] p_stocks, int[] p_ids, HttpSession session) {

		User user = (User) session.getAttribute("loginedUser");
		Customer customer = (Customer) session.getAttribute("customerI");

		int i = p_ids.length;
		
		

		for (int j = 0; j < i; j++) {

			int p_stock = p_stocks[j];
			int p_id = p_ids[j];

			product.setS_id(user.getU_store());
			product.setU_id(user.getU_id());
			product.setP_id(p_id);
			product.setP_stock(p_stock);

			service.removeStocks(product);
			service.saleProducts(product);
			
			if(customer != null) {
			cservice.BuyCustomer(customer, product);
			session.setAttribute("customerI", null);
			}
			else {
				return "redirect:/list.do";
			}
			
		}
		return "redirect:/list.do";
	}

	@RequestMapping(value = "select.do", method = RequestMethod.GET)
	public ModelAndView SelectProducts(HttpSession session) {

		User user = (User) session.getAttribute("loginedUser");

		List<Product> selectList = service.selectAll(user);
		ModelAndView modelAndView = new ModelAndView("select");

		modelAndView.addObject("selectList", selectList);

		return modelAndView;
	}

	@RequestMapping(value = "select.do", method = RequestMethod.POST)
	public String SendProducts(Product product, int[] p_stocks, int[] p_ids, HttpSession session) {

		User user = (User) session.getAttribute("loginedUser");

		int i = p_ids.length;

		for (int j = 0; j < i; j++) {

			int p_stock = p_stocks[j];
			int p_id = p_ids[j];

			product.setU_id(user.getU_id());
			product.setP_id(p_id);
			product.setS_id(user.getU_store());
			product.setP_stock(p_stock);

			service.sendProducts(product);
		}
		return "redirect:/order.do";
	}

	@RequestMapping(value = "select2.do", method = RequestMethod.POST)
	public String addStock(Product product, int[] p_ids, HttpSession session) {

		User user = (User) session.getAttribute("loginedUser");

		for (int p_id : p_ids) {

			product.setU_id(user.getU_id());
			product.setP_id(p_id);

			service.deleteSeletions(product);

		}
		return "redirect:/select.do";
	}

	@RequestMapping("searchByName.do")
	public ModelAndView productByName(Model model, @RequestParam("p_nameList") String name,
			@RequestParam("u_storeList") int s_id, Criteria cri, HttpSession session) {
		
		Customer customer = (Customer) session.getAttribute("customerI");

		ModelAndView modelAndView = new ModelAndView("main");

		List<Product> list = service.SearchByName(name, s_id, cri);

		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		pageMaker.setTotalCount(100);
		
		modelAndView.addObject("customer", customer);
		modelAndView.addObject("pageMaker", pageMaker);
		modelAndView.addObject("productList", list);
		modelAndView.addObject("storeid", s_id);

		return modelAndView;
	}

	@RequestMapping(value = "order.do", method = RequestMethod.GET)
	public ModelAndView OrderProducts(HttpSession session) {
		User user = (User) session.getAttribute("loginedUser");

		List<Product> orderList = service.orderAll(user.getU_store());
		ModelAndView modelAndView = new ModelAndView("order");

		modelAndView.addObject("orderList", orderList);

		return modelAndView;
	}

	@RequestMapping(value = "sale.do", method = RequestMethod.GET)
	public ModelAndView SaleProducts(HttpSession session) {
		User user = (User) session.getAttribute("loginedUser");

		List<Product> saleList = service.saleAll(user.getU_store());
		ModelAndView modelAndView = new ModelAndView("sale");

		modelAndView.addObject("saleList", saleList);

		return modelAndView;
	}

	@RequestMapping("searchCustomer.do")
	public ModelAndView SearchCustomer(Model model, @RequestParam("customer") int phone, Criteria cri,
			HttpSession session) {

		User user1 = (User) session.getAttribute("loginedUser");

		int s_id = user1.getU_store();

		ModelAndView modelAndView = new ModelAndView("main");
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		pageMaker.setTotalCount(159);

		Customer customer = cservice.searchByPhone(phone);
		List<Product> list = service.readAll(s_id, cri);

		session.setAttribute("customerI", customer);

		modelAndView.addObject("customer", customer);
		modelAndView.addObject("productList", list);
		modelAndView.addObject("storeNum", s_id);
		modelAndView.addObject("pageMaker", pageMaker);

		return modelAndView;
	}
	
	@RequestMapping("searchByDate.do")
	   public ModelAndView saleByDate(@RequestParam("date") Date sa_date, HttpSession session, Product product) {
	      User user = (User) session.getAttribute("loginedUser");

	      int s_id = user.getU_store();

	      ModelAndView modelAndView = new ModelAndView("sale");
	      List<Product> saleList = service.SearchByDate(sa_date, s_id);
	      modelAndView.addObject("saleList", saleList);

	      return modelAndView;
	      
	   }


}
